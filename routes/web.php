<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group([
    'prefix' => 'regions',
], function () {
    Route::get('/', 'App\Http\Controllers\RegionsController@index')
         ->name('regions.regions.index');
    Route::get('/create','App\Http\Controllers\RegionsController@create')
         ->name('regions.regions.create');
    Route::get('/show/{regions}','App\Http\Controllers\RegionsController@show')
         ->name('regions.regions.show');
    Route::get('/{regions}/edit','App\Http\Controllers\RegionsController@edit')
         ->name('regions.regions.edit');
    Route::post('/', 'App\Http\Controllers\RegionsController@store')
         ->name('regions.regions.store');
    Route::put('regions/{regions}', 'App\Http\Controllers\RegionsController@update')
         ->name('regions.regions.update');
    Route::delete('/regions/{regions}','App\Http\Controllers\RegionsController@destroy')
         ->name('regions.regions.destroy');
});

Route::group([
    'prefix' => 'assets',
], function () {
    Route::get('/', 'App\Http\Controllers\AssetsController@index')
         ->name('assets.asset.index');
    Route::get('/create','App\Http\Controllers\AssetsController@create')
         ->name('assets.asset.create');
    Route::get('/show/{asset}','App\Http\Controllers\AssetsController@show')
         ->name('assets.asset.show')->where('id', '[0-9]+');
    Route::get('/{asset}/edit','App\Http\Controllers\AssetsController@edit')
         ->name('assets.asset.edit')->where('id', '[0-9]+');
    Route::post('/', 'App\Http\Controllers\AssetsController@store')
         ->name('assets.asset.store');
    Route::put('asset/{asset}', 'App\Http\Controllers\AssetsController@update')
         ->name('assets.asset.update')->where('id', '[0-9]+');
    Route::delete('/asset/{asset}','App\Http\Controllers\AssetsController@destroy')
         ->name('assets.asset.destroy')->where('id', '[0-9]+');
});

Route::group([
    'prefix' => 'asset_categories',
], function () {
    Route::get('/', 'App\Http\Controllers\AssetCategoriesController@index')
         ->name('asset_categories.asset_category.index');
    Route::get('/create','App\Http\Controllers\AssetCategoriesController@create')
         ->name('asset_categories.asset_category.create');
    Route::get('/show/{assetCategory}','App\Http\Controllers\AssetCategoriesController@show')
         ->name('asset_categories.asset_category.show')->where('id', '[0-9]+');
    Route::get('/{assetCategory}/edit','App\Http\Controllers\AssetCategoriesController@edit')
         ->name('asset_categories.asset_category.edit')->where('id', '[0-9]+');
    Route::post('/', 'App\Http\Controllers\AssetCategoriesController@store')
         ->name('asset_categories.asset_category.store');
    Route::put('asset_category/{assetCategory}', 'App\Http\Controllers\AssetCategoriesController@update')
         ->name('asset_categories.asset_category.update')->where('id', '[0-9]+');
    Route::delete('/asset_category/{assetCategory}','App\Http\Controllers\AssetCategoriesController@destroy')
         ->name('asset_categories.asset_category.destroy')->where('id', '[0-9]+');
});
