@extends('layouts.app')

@section('content')

<div class="panel panel-default">
    <div class="panel-heading clearfix">

        <span class="pull-left">
            <h4 class="mt-5 mb-5">{{ isset($title) ? $title : 'Regions' }}</h4>
        </span>

        <div class="pull-right">

            <form method="POST" action="{!! route('regions.regions.destroy', $regions->id) !!}" accept-charset="UTF-8">
            <input name="_method" value="DELETE" type="hidden">
            {{ csrf_field() }}
                <div class="btn-group btn-group-sm" role="group">
                    <a href="{{ route('regions.regions.index') }}" class="btn btn-primary" title="Show All Regions">
                        <span class="glyphicon glyphicon-th-list" aria-hidden="true"></span>
                    </a>

                    <a href="{{ route('regions.regions.create') }}" class="btn btn-success" title="Create New Regions">
                        <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                    </a>
                    
                    <a href="{{ route('regions.regions.edit', $regions->id ) }}" class="btn btn-primary" title="Edit Regions">
                        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                    </a>

                    <button type="submit" class="btn btn-danger" title="Delete Regions" onclick="return confirm(&quot;Click Ok to delete Regions.?&quot;)">
                        <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                    </button>
                </div>
            </form>

        </div>

    </div>

    <div class="panel-body">
        <dl class="dl-horizontal">
            <dt>Region Name</dt>
            <dd>{{ $regions->region_name }}</dd>

        </dl>

    </div>
</div>

@endsection