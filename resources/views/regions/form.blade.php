
<div class="form-group {{ $errors->has('region_name') ? 'has-error' : '' }}">
    <label for="region_name" class="col-md-2 control-label">Region Name</label>
    <div class="col-md-10">
        <input class="form-control" name="region_name" type="text" id="region_name" value="{{ old('region_name', optional($regions)->region_name) }}" maxlength="255" placeholder="Enter region name here...">
        {!! $errors->first('region_name', '<p class="help-block">:message</p>') !!}
    </div>
</div>

