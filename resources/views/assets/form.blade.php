
<div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
    <label for="name" class="col-md-2 control-label">Name</label>
    <div class="col-md-10">
        <input class="form-control" name="name" type="text" id="name" value="{{ old('name', optional($asset)->name) }}" minlength="1" maxlength="255" placeholder="Enter name here...">
        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('asset_categories_id') ? 'has-error' : '' }}">
    <label for="asset_categories_id" class="col-md-2 control-label">Asset Categories</label>
    <div class="col-md-10">
        <select class="form-control" id="asset_categories_id" name="asset_categories_id">
        	    <option value="" style="display: none;" {{ old('asset_categories_id', optional($asset)->asset_categories_id ?: '') == '' ? 'selected' : '' }} disabled selected>Select asset categories</option>
        	@foreach ($assetCategories as $key => $assetCategory)
			    <option value="{{ $key }}" {{ old('asset_categories_id', optional($asset)->asset_categories_id) == $key ? 'selected' : '' }}>
			    	{{ $assetCategory }}
			    </option>
			@endforeach
        </select>
        
        {!! $errors->first('asset_categories_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('notes') ? 'has-error' : '' }}">
    <label for="notes" class="col-md-2 control-label">Notes</label>
    <div class="col-md-10">
        <textarea class="form-control" name="notes" cols="50" rows="10" id="notes" minlength="1" maxlength="1000">{{ old('notes', optional($asset)->notes) }}</textarea>
        {!! $errors->first('notes', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('created_by') ? 'has-error' : '' }}">
    <label for="created_by" class="col-md-2 control-label">Created By</label>
    <div class="col-md-10">
        <select class="form-control" id="created_by" name="created_by">
        	    <option value="" style="display: none;" {{ old('created_by', optional($asset)->created_by ?: '') == '' ? 'selected' : '' }} disabled selected>Select created by</option>
        	@foreach ($creators as $key => $creator)
			    <option value="{{ $key }}" {{ old('created_by', optional($asset)->created_by) == $key ? 'selected' : '' }}>
			    	{{ $creator }}
			    </option>
			@endforeach
        </select>
        
        {!! $errors->first('created_by', '<p class="help-block">:message</p>') !!}
    </div>
</div>

