<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\regions;
use Illuminate\Http\Request;
use Exception;

class RegionsController extends Controller
{

    /**
     * Display a listing of the regions.
     *
     * @return Illuminate\View\View
     */
    public function index()
    {
        $regionsObjects = regions::paginate(25);

        return view('regions.index', compact('regionsObjects'));
    }

    /**
     * Show the form for creating a new regions.
     *
     * @return Illuminate\View\View
     */
    public function create()
    {
        
        
        return view('regions.create');
    }

    /**
     * Store a new regions in the storage.
     *
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        try {
            
            $data = $this->getData($request);
            
            regions::create($data);

            return redirect()->route('regions.regions.index')
                ->with('success_message', 'Regions was successfully added.');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }

    /**
     * Display the specified regions.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function show($id)
    {
        $regions = regions::findOrFail($id);

        return view('regions.show', compact('regions'));
    }

    /**
     * Show the form for editing the specified regions.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function edit($id)
    {
        $regions = regions::findOrFail($id);
        

        return view('regions.edit', compact('regions'));
    }

    /**
     * Update the specified regions in the storage.
     *
     * @param int $id
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        try {
            
            $data = $this->getData($request);
            
            $regions = regions::findOrFail($id);
            $regions->update($data);

            return redirect()->route('regions.regions.index')
                ->with('success_message', 'Regions was successfully updated.');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }        
    }

    /**
     * Remove the specified regions from the storage.
     *
     * @param int $id
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        try {
            $regions = regions::findOrFail($id);
            $regions->delete();

            return redirect()->route('regions.regions.index')
                ->with('success_message', 'Regions was successfully deleted.');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }

    
    /**
     * Get the request's data from the request.
     *
     * @param Illuminate\Http\Request\Request $request 
     * @return array
     */
    protected function getData(Request $request)
    {
        $rules = [
                'region_name' => 'nullable|string|min:0|max:255', 
        ];

        
        $data = $request->validate($rules);




        return $data;
    }

}
